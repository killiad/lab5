/*Killian Davis
  killiad
  Lab 5 and Lab 6
  Lab Section: 4
  Anurata Hridi*/

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include "stdlib.h"
#include "string.h"

using namespace std;
enum Suit { SPADES=0, HEARTS=1, DIAMONDS=2, CLUBS=3 };

typedef struct Card {
  Suit suit;
  int value;
} Card;

string get_suit_code(Card& c);
string get_card_name(Card& c);
bool suit_order(const Card& lhs, const Card& rhs);
int myrandom (int i) { return std::rand()%i;}


int main(int argc, char const *argv[]) {
  // IMPLEMENT as instructed below
  /*This is to seed the random generator */
  srand(unsigned (time(0)));

  /*Create a deck of cards of size 52 (hint this should be an array) and
   *initialize the deck*/

  /*Initialization of deck assigns numbers first, then suits*/
   Card deck[52];
   int counter = 0;
	   for (int i = 0; i < 4; i++){
		 for (int j = 2; j <= 14; j++){
			deck[counter].suit = static_cast<Suit>(i);
			deck[counter].value = j;
			counter++;
		}
   }

  /*After the deck is created and initialzed we call random_shuffle() see the
   *notes to determine the parameters to pass in.*/

	random_shuffle(&deck[0], &deck[52], *myrandom);

   /*Build a hand of 5 cards from the first five cards of the deck created
    *above*/
	Card hand[5] = {deck[0], deck[1], deck[2], deck[3], deck[4]};

    /*Sort the cards.  Links to how to call this function is in the specs
     *provided*/
	sort(&hand[0], &hand[5], suit_order);

    /*Now print the hand below. You will use the functions get_card_name and
     *get_suit_code */ 
	
	/*Pieces together the name of the card, formats, then prints*/
	for(int i = 0; i < 5; i++){
		string name = get_card_name(hand[i]);
		string code = get_suit_code(hand[i]);
		cout << setw(10) << name << " of " << code << endl;
  }


  return 0;
}


/*This function will be passed to the sort funtion. Hints on how to implement
* this is in the specifications document.*/

/*INPUT:    left card, right card
  FUNCTION: Determines if the left card is smaller than or equal
            to right card
  OUTPUT:	Boolean value*/
bool suit_order(const Card& lhs, const Card& rhs) {
  if(lhs.suit < rhs.suit){
  	return true;
  }
  else if (lhs.suit == rhs.suit){
  	return lhs.value < rhs.value;
	}
  else {return false;}
}

string get_suit_code(Card& c) {
  switch (c.suit) {
    case SPADES:    return "\u2660";
    case HEARTS:    return "\u2661";
    case DIAMONDS:  return "\u2662";
    case CLUBS:     return "\u2663";
    default:        return "";
  }
}

/*INPUT:    a card
  FUNCTION: Returns the name of the card (checks for face cards)
  OUTPUT:	a string that holds the card name*/
string get_card_name(Card& c) {
  switch(c.value) {
    case 11:      return "Jack";
    case 12:      return "Queen";
    case 13:      return "King";
    case 14:      return "Ace";
	default:      return to_string(c.value);
  }
}
